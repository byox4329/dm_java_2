import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.*;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if (liste.isEmpty()){
            return null;
        }
        Integer min = liste.get(0);
        for (Integer val : liste){
            if (val < min){
                min = val;
            }
        }
        return min;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for ( T val : liste){
            if (val.compareTo(valeur) <=0){
                return false;
            }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> listefinale = new ArrayList<>();
        int i = 0;
        int j = 0;
        while (i<liste1.size() && j<liste2.size()){
            T objet = liste1.get(i);
            int compare = objet.compareTo(liste2.get(j));
            if (compare == 0) {
                if (!listefinale.contains(objet))
                    listefinale.add(objet);
                i++;
                j++;
            } else if (compare > 0) {
                j++;
            } else {
                i++;
            }
        }
        return listefinale;
    }

    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettbienparenthese de l'alphabet et des espaces.
     * @param texte une chaine de caractèbienparenthese
     * @return une liste de mots, corbienparenthesepondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){

    List<String> listemots = new ArrayList<>();
        if(texte.length() == 0){
          return listemots;
        }
        String motencours = "";
        for(int i = 0; i < texte.length(); i++){
          Character cara = texte.charAt(i);
          String car = cara.toString();
          if(car.compareTo(" ") == 0){
            if(motencours.length() > 0){
              listemots.add(motencours);
              motencours = "";
            }
          }
          else{
            motencours = motencours + car;
          }
        }
        if(motencours != ""){
          listemots.add(motencours);
        }
      return listemots;
    }



    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractèbienparenthese
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if(texte.length() == 0){
          return null;}
        Map<String,Integer> dico = new HashMap<>();
        List<String> listemots;
        listemots = decoupe(texte);
        for (String mot : listemots){
            if (dico.containsKey(mot)){
                dico.put(mot,dico.get(mot)+1);
            }else{
                dico.put(mot,1);
            }
        }
        Set<String> ensemblecle = dico.keySet();
        String motmax = "" ;
        int nbapparitionmax = 0;
        for (String mot : ensemblecle){
            if (motmax == ""){
                motmax = mot;
                nbapparitionmax = dico.get(mot);
            }else if (dico.get(mot) > nbapparitionmax){
                motmax = mot;
                nbapparitionmax = dico.get(mot);
            }else if (dico.get(mot) == nbapparitionmax){
                if (motmax.compareTo(mot) > 0){
                    motmax = mot;
                }
            }
        }
        return motmax;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractèbienparenthese composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int equilibre = 0;
        for (int i =0; i < chaine.length(); i++){
            Character val = chaine.charAt(i);
            if (equilibre < 0){return false;}
            if (val == '('){
                equilibre++;
            }else if (val == ')'){
                equilibre--;
            }
        }
        return (equilibre==0);
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractèbienparenthese composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        int cdroit = 0;
        int cgauche = 0;
        int pgauche = 0;
        int pdroit = 0;
        boolean bienparenthese = true;
        int i = 0;

        if(chaine.isEmpty()){
            return true;
        }
        else if(chaine.length() == 1){
            return false;
        }
        else {
            while(i < chaine.length() && bienparenthese){
                if(chaine.charAt(i)=='('){
                    pgauche += 1;
                    if(i == chaine.length() - 1 || (i < chaine.length() - 1 && chaine.charAt(i + 1) == ']')){
                        bienparenthese = false;
                    }
                }else if(chaine.charAt(i)==')'){
                    pdroit += 1;
                    if(i == 0 || (i > chaine.length() - 1 && chaine.charAt(i - 1) == '[')){
                        bienparenthese = false;
                    }
                }else if(chaine.charAt(i)=='['){
                    cgauche += 1;
                    if(i == chaine.length() - 1 || (i < chaine.length() - 1 && chaine.charAt(i + 1) == ')')){
                        bienparenthese = false;
                    }
                }else if(chaine.charAt(i)==']'){
                    cdroit += 1;
                    if(i == 0 || (i > chaine.length() - 1 && chaine.charAt(i - 1) == '(')){
                        bienparenthese = false;
                    }
                }
                i += 1;
            }
            if(pgauche != pdroit || cgauche != cdroit){
                bienparenthese = false;
            }
        }
        return bienparenthese;   
    }

    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.isEmpty()){return false;}
        if (valeur<liste.get(0) || valeur>liste.get(liste.size()-1)){return false;}
        int debut = 0;
        int fin = liste.size()-1;
        int indice = 0;
        while(debut < fin){
        if(liste.get(indice).compareTo(valeur) > 0){
            fin = indice -1 ;
        }else if(liste.get(indice).compareTo(valeur) < 0){
            debut = indice + 1;
        }
        indice = (debut + fin) / 2;
        if(liste.get(indice).compareTo(valeur) == 0){
            return true;}        
        }
        return false;
    }
}
